
<link href="https://fonts.googleapis.com/css?family=Abel&display=swap" rel="stylesheet">

<footer class="clearfix" style="background-image:url({{url('images/footer.jpg')}}); background-position-y: center; background-repeat: no-repeat; background-size: cover; border-top:0px;margin-top:0px;">
  <div class="container pt-3 border-bottom">
    <div class="row">
      <div class="col-md-3 col-sm-12 mb-3 text-center">
        <img src="{{url('images/retina-portfolio.png')}}" style="max-width: 200px; margin-left:-80px;margin-top: 30%;">   
      </div>

      <div class="col-md-9 col-sm-12">

        <div class="col-md-3 col-sm-6 col-6 p-0 float-left mb-3">
        <ul class="list-group" style="color:rgb(245, 206, 13);line-height:10px;">
          <li ><a style="color:rgb(245, 206, 13);" href=""><h5 style="color:rgb(245, 206, 13);letter-spacing:3px;font-family: 'Abel', sans-serif;">INICIO</h5></a></li>
          <li ><a style="color:rgb(245, 206, 13);" href=""><h5 style="color:rgb(245, 206, 13);letter-spacing:3px;font-family: 'Abel', sans-serif;">NOSOTROS</h5></a></li>
        <li class="list-group-item bg-transparent border-0 p-0 mb-3"><a style="color:rgb(179, 179, 174);letter-spacing:3px;font-weight:700;" href="">portafolio</a></li>
          <li ><a style="color:rgb(245, 206, 13);" href=""><h5 style="color:rgb(245, 206, 13);letter-spacing:3px;font-family: 'Abel', sans-serif;">SERVICIOS</h5></a></li>
          <li ><a style="color:rgb(245, 206, 13);" href=""><h5 style="color:rgb(245, 206, 13);letter-spacing:3px;font-family: 'Abel', sans-serif;">CONTACTO</h5></a></li>
        </ul>
        </div>

        <div class="col-md-3 col-sm-6 col-6 p-0 mb-3 float-left" style="margin-left: 80px!important;">
          <h5 class="mb-4 font-weight-bold text-uppercase" style="color:rgb(245, 206, 13);letter-spacing:3px;font-family: 'Abel', sans-serif;">MONTERREY</h5>
        <ul class="list-group">
        <li class="list-group-item bg-transparent border-0 p-0 mb-2"><img src="{{url('images/phone.png')}}" style="max-width: 20px;"><a style="color:rgb(179, 179, 174);font-weight:700;" href="">  (818)-0-00-72-88</a></li>
          <li class="list-group-item bg-transparent border-0 p-0 mb-2"><img src="{{url('images/ubicacion.png')}}" style="max-width: 20px;"><a style="color:rgb(179, 179, 174);font-weight:700;" href=""> Torre C IOS Campestre <br>Ricardo Margain Zozaya</a></li>
        </ul>
        </div>


        <div class="col-md-3 col-sm-6 col-6 p-0 mb-3 float-right">
          <h5 class="mb-4 font-weight-bold text-uppercase" style="color:rgb(245, 206, 13);letter-spacing:3px;font-family: 'Abel', sans-serif;">PUEBLA (MATRIZ)</h5>
        <ul class="list-group">
          <li class="list-group-item bg-transparent border-0 p-0 mb-2">
            <img src="{{url('images/world.png')}}" style="max-width: 20px;"><a href="www.animatiomx.com" style="color:rgb(179, 179, 174);font-weight:300;"> www.animatiomx.com</a>
          </li>
          <li class="list-group-item bg-transparent border-0 p-0 mb-2">
            <img src="{{url('images/phone.png')}}" style="max-width: 20px;"><a href=""style="color:rgb(179, 179, 174);font-weight:700;"> (222)-1-78-25-65</a>       
          </li class="list-group-item bg-transparent border-0 p-0 mb-2">
          <li class="list-group-item bg-transparent border-0 p-0 mb-2"> 
            <img src="{{url('images/ubicacion.png')}}" style="max-width: 20px;"><a href="" style="color:rgb(179, 179, 174);font-weight:700;"> 14 Ote. #604 Int.6 <br>
            San Ándres Cholula, Pue.</a>
          </li>
        </ul>
        </div>
      </div>

        <div class="col-md-12">
          <div class="d-flex" style="display: flex; float: right!important;">
            <a class="mr-4" href="../privacy.html" style="color:rgb(179, 179, 174);">©2019 Todos los derechos reservados</a>
            <a href="www.animatiomx.com" style="color:rgb(245, 206, 13);">Animatiomx</a>
          </div>
        </div>

    </div>
  </div>
</footer>