<!-- 
        <header id="header" class="" style="background-color:black;height:60px;">

  

          <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                  
                  <div id="logo">
                        <a href=""><img src="{{asset('images/retina-portfolio.png')}}" alt="Canvas Logo" style="height: 30px;"></a>
                    </div>


           <nav id="primary-menu" style="background-color:black;height:60px;">

                 <ul>
                       
                        @guest
            <li class="nav-item"><a style="color:white" class="nav-link"href="{{ url('admin/posts') }}">Inicio</a> </li>
            <li class="nav-item"><a style="color:white" class="nav-link"href="{{ url('admin/categories') }}">Nosotros</a> </li>
            <li class="nav-item"><a style="color:white" class="nav-link"href="{{ url('admin/comments') }}">Servicios</a> </li>
            <li class="nav-item"><a style="color:white" class="nav-link"href="{{ url('admin/tags') }}">Promociones</a> </li>
            <li class="nav-item"><a style="color:white" class="nav-link"href="{{ url('admin/tags') }}">Contacto</a> </li>
            <li class="nav-item"> <a style="color:white" class="nav-link"href="{{ route('login') }}">Blog</a> </li>
            <li class="nav-item"><a style="color:white" class="nav-link" target="_blank" href="https://siacademico.com"><span><img src="{{asset('images/logo-sia.png')}}" width="50" style="margin-top: -10px;"></span></a> </li>
            <li class="nav-item"><a style="color:white" class="nav-link" target="_blank" href="https://wa.me/528121094312"><span><img src="{{asset('images/logo-whats.png')}}" width="30" style="margin-top: -5px;"></span></a> </li>
                        @else
                <li class="nav-item"><a style="color:white" class="nav-link" href="{{route('tags.index')}}">Etiquetas</a></li>
                <li class="nav-item"><a style="color:white" class="nav-link" href="{{route('categories.index')}}">Categorias</a></li>
                <li class="nav-item"><a style="color:white" class="nav-link" href="{{route('posts.index')}}">Publicaciones</a></li>
                
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" style="color:white" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                            </li>
                            <li><a style="color:white" class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Cerrar Sesión') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                    </li>
                        @endguest
                    </ul>
                </nav>


    </header>
             -->

<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" style="background-color:black!important;height:60px;">
 <a class="navbar-brand" href="{{url('blog')}}" style="margin-left:6%">
                <img src="{{url('images/retina-portfolio.png')}}" width="100" height="30" alt="">
                @includeWhen(Auth::user(), 'layouts.blo')
                </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
<style>
  #menu{
    font-size:115%;
    margin-right:6%;
  }
  #menu li{
padding-right:3%;
  }
</style>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul id="menu" class="navbar-nav ml-auto mt-3">
      <!-- <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
      </li> -->
       @guest
            <li class="nav-item"><a style="color:white" class="nav-link"href="{{ url('admin/posts') }}">Inicio</a> </li>
            <li class="nav-item"><a style="color:white" class="nav-link"href="{{ url('admin/categories') }}">Nosotros</a> </li>
            <li class="nav-item"><a style="color:white" class="nav-link"href="{{ url('admin/comments') }}">Servicios</a> </li>
            <li class="nav-item"><a style="color:white" class="nav-link"href="{{ url('admin/tags') }}">Promociones</a> </li>
            <li class="nav-item"><a style="color:white" class="nav-link"href="{{ url('admin/tags') }}">Contacto</a> </li>
            <li class="nav-item"> <a style="color:white" class="nav-link"href="{{ route('login') }}">Blog</a> </li>
            <li class="nav-item"><a style="color:white" class="nav-link" target="_blank" href="https://siacademico.com"><span><img src="{{url('images/logo-sia.png')}}" width="50" style="margin-top: -10px;"></span></a> </li>
            <li class="nav-item"><a style="color:white" class="nav-link" target="_blank" href="https://wa.me/528121094312"><span><img src="{{url('images/logo-whats.png')}}" width="30" style="margin-top: -5px;"></span></a> </li>
                        @else
                 <li class="nav-item"><a style="color:white" class="nav-link" href="{{route('posts.index')}}">Publicaciones</a></li>
                <li class="nav-item"><a style="color:white" class="nav-link" href="{{route('categories.index')}}">Categorias</a></li>
                <li class="nav-item"><a style="color:white" class="nav-link" href="{{route('tags.index')}}">Etiquetas</a></li>            
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" style="color:white" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a  class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Cerrar Sesión') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                                
                            </li>
                        @endguest
    </ul>
   <!--  <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form> -->
  </div>
</nav>