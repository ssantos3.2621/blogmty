<style type="text/css">
    .titulos-footer{
        font-size: 15px;
        color: yellow !important; 
        font-weight: bold; 
        font-family: 'Abel', sans-serif !important; 
        letter-spacing: 5px;
    }
</style>

<footer class="clearfix" style="background-image:url({{url('images/footer.jpg')}}); background-position-y: center; background-repeat: no-repeat; background-size: cover; border-top:0px;margin-top:0px;">
    
    <!-- <div class="widgets_wrapper" style="padding:20px 0"> -->
        <div class="container d-flex pt-4" style="line-height:25px;">

            <div class="col-md-3 mr-10 mb-10" style="margin: 3% 0%;">
                <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                    <div class="image_wrapper">
                        <img class="scale-with-grid" src="{{url('images/retina-portfolio.png')}}" alt="logo-Animatiomx-MTY" width="200">
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="column_attr align_left">
                    <ul style="color: white!important;line-height:30px;">
                        <li>
                            <a href="{{url('/')}}" class="titulos-footer">INICIO</a>
                        </li>
                        <li>
                            <a href="{{url('nosotros')}}" class="titulos-footer">NOSOTROS</a>
                            <ol>
                                <li style="list-style: none;font-size: 15px; font-weight: bold; font-family: 'Abel', sans-serif !important; letter-spacing: 5px;color: #737874;">Portafolio</li>
                            </ol>
                        </li>
                        <li>
                            <a href="{{url('servicios')}}" class="titulos-footer">SERVICIOS</a>
                        </li>
                        <li>
                            <a href="{{url('contacto')}}" class="titulos-footer">CONTACTO</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-3">
                <div class="column_attr align_left">
                    <ul>
                        <h1 class="titulos-footer">MONTERREY</h1>
                        <div class="column one column_column" style="color:#ffff!important; margin-left: -30px;font-weight:bold;">
                            <div class="column one column_column" style="margin-bottom: 10px">
                                <img src="{{url('images/phone.png')}}" width="20" style="float: left; margin-right: 10px">
                                <label style="color:#B3B3AE!important;font-weight:bold;">(818) · 0 · 00 · 72 · 88</label>
                            </div>
                            <div class="column one column_column" style="margin-bottom: 10px">
                                <img src="{{url('images/ubicacion.png')}}" width="20" style="float: left;">
                                <label style="margin-left:10px;color:#B3B3AE!important;font-weight:bold;">Torre C IOS Campestre<br></label>
                                <label style="margin-left:30px;color:#B3B3AE!important;font-weight:bold;  ">Ricardo Margain Zozaya</label>
                            </div>
                        </div>
                    </ul>
                </div>
            </div>

            <div class="col-md-3">
                <div class="column_attr align_left">
                    <ul>
                        <h1 class="titulos-footer">PUEBLA (MATRIZ)</h1>
                        <div class="column one column_column" style="color: #b3b3b3; margin-left: -30px;">
                            <div class="column one column_column" style="margin-bottom: 10px">
                                <img src="{{url('images/world.png')}}" width="20" style="float: left; margin-right: 10px">
                                <a href="http://animatiomx.com" target="_blank" style="color:#B3B3AE!important;font-weight:bold;">www.animatiomx.com</a>
                            </div>
                            <div class="column one column_column" style="margin-bottom: 10px">
                                <img src="{{url('images/phone.png')}}" width="20" style="float: left; margin-right: 10px">
                                <label style="color:#B3B3AE!important;font-weight:bold;">(222) · 1 · 78 · 25 · 65</label>
                            </div>
                            <div class="column one column_column" style="margin-bottom: 10px">
                                <img src="{{url('images/ubicacion.png')}}" width="20" style="float: left;">
                                <label style="margin-left:10px;color:#B3B3AE!important;font-weight:bold;">14 Ote. # 604 Int. 6<br></label>
                                <label style="margin-left:30px;color:#B3B3AE!important;font-weight:bold;">San Ándres Cholula, Pue.</label>
                            </div>
                        </div>
                    </ul>
                </div>
            </div> 
        </div>
        <div class="col-md-12">
                <div class="float-right">
                    <div class="mr-5" style="margin-top:-30px;">&copy; 2019 Todos los derechos reservados   
                        <a target="_blank" rel="nofollow" href="http://animatiomx.com" style="color: #efd101">
                            Animatiomx
                        </a>
                    </div>
                </div>
            </div>
 <!--    </div> -->
</footer>
