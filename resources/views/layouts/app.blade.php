<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets
    ============================================= -->
    <link href="{{ asset('https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/dark.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/font-icons.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}" type="text/css" />

    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Document Title
    ============================================= -->
    <title>Blog - Grid 3 Columns | Canvas</title>

</head>

<body class="stretched">

    <!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix">


<header id="header" class="full-header" style="background-color:black;">

      <div id="header-wrap">

        <div class="container clearfix">

          <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

          <!-- Logo
          ============================================= -->
          <div id="logo">
            <a href="" class="standard-logo" ><img src="{{asset('images/retina-portfolio.png')}}" alt="Canvas Logo"></a>
          </div>

           <nav id="primary-menu">

                 <ul>
                        <!-- Authentication Links -->
                        @guest
            <li class="nav-item"><a style="color:white" class="nav-link"href="{{ url('admin/posts') }}">Inicio</a> </li>
            <li class="nav-item"><a style="color:white" class="nav-link"href="{{ url('admin/categories') }}">Nosotros</a> </li>
            <li class="nav-item"><a style="color:white" class="nav-link"href="{{ url('admin/comments') }}">Servicios</a> </li>
            <li class="nav-item"><a style="color:white" class="nav-link"href="{{ url('admin/tags') }}">Promociones</a> </li>
            <li class="nav-item"><a style="color:white" class="nav-link"href="{{ url('admin/tags') }}">Contacto</a> </li>
            <li class="nav-item"> <a style="color:white" class="nav-link"href="{{ route('login') }}">Blog</a> </li>
            <li class="nav-item"><a style="color:white" class="nav-link" target="_blank" href="https://siacademico.com"><span><img src="{{url('images/logo-sia.png')}}" width="50" style="margin-top: -10px;"></span></a> </li>
            <li class="nav-item"><a style="color:white" class="nav-link" target="_blank" href="https://wa.me/528121094312"><span><img src="{{url('images/logo-whats.png')}}" width="30" style="margin-top: -5px;"></span></a> </li>
                        @else
                <li class="nav-item"><a style="color:white" class="nav-link" href="{{route('tags.index')}}">Etiquetas</a></li>
                <li class="nav-item"><a style="color:white" class="nav-link" href="{{route('categories.index')}}">Categorias</a></li>
                <li class="nav-item"><a style="color:white" class="nav-link" href="{{route('posts.index')}}">Publicaciones</a></li>
                
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" style="color:white" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
<!-- 
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    
                                </div> -->
                                
                            </li>
                            <li><a style="color:white" class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Cerrar Sesi贸n') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                    </li>
                        @endguest
                    </ul>
                </nav><!-- #primary-menu end -->

        </div>

      </div>

    </header>

        <section id="content">

            @yield('content') 

        </section><!-- #content end -->

    
    </div><!-- #wrapper end -->
    <footer>
    @include('layouts.footer')
    </footer>

    <!-- Go To Top
    ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <!-- External JavaScripts
    ============================================= -->
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/plugins.js') }}"></script>

    <!-- Footer Scripts
    ============================================= -->
    <script src="{{ asset('js/functions.js') }}"></script>

</body>
</html>