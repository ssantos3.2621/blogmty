<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets
    ============================================= -->
    <link href="{{asset('https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/dark.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/font-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/animate.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}" type="text/css" />

    <link rel="stylesheet" href="{{asset('css/responsive.css')}}" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="stylesheet" href="{{asset('css/custom.css')}}" type="text/css" />

    <!-- Document Title
    ============================================= -->
    <title>Blog</title>

<style>
.gradient-button {
    background: rgb(131,58,180);
    background: linear-gradient(90deg, rgba(131,58,180,1) 0%, rgba(253,29,29,1) 50%, rgba(252,176,69,1) 100%);
}
</style>

</head>

<body class="stretched" style="border:0px;margin:0px;">

    <!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix" >
@include('layouts.header')  
<section id="content">

<div class="content-wrap nopadding">

 <div class="section full-screen nopadding nomargin" style="background: url('images/ondas1.png')
    center center no-repeat;background-size: cover;">
      <div class="container-fluid vertical-middle divcenter clearfix">
          <div class="row d-flex justify-content-center" style="font-size: 100%;">

      <div class="card mb-6 shadow" style="width: 300px;">
        <div class="row no-gutters">
          <div class="col-md-4" style="background-color: rgb(51, 51, 51);">
            <img src="images/documentos.png" class="card-img" style="width: 80%;margin-top:40%;margin-left:10%">
              </div>
              <div class="col-md-8">
              <div class="card-body" style="">
               <h1 style="font-size: 320%; display: flex;justify-content: center;">{{ $posts }}</h1>
              <h4 class="card-title" style="font-size: 150%;display: flex;justify-content: center;">Publicaciones</h4>
              </div>
            </div>
          </div>
        </div>

        <div class="container d-flex justify-content-center mt-4">

            <div class="card mb-6 mr-4 shadow" style="width: 300px;">
              <div class="row no-gutters">
                <div class="col-md-4" style="background-color: rgb(51, 51, 51);">
                  <img src="images/etiquetas.png" class="card-img" style="width: 80%;margin-top:40%;margin-left:10%">
                    </div>
                    <div class="col-md-8">
                    <div class="card-body" style="">
                     <h1 style="font-size: 320%; display: flex;justify-content: center;">{{ $tags }}</h1>
                    <h4 class="card-title" style="font-size: 150%;display: flex;justify-content: center;">Etiquetas</h4>
                    </div>
                  </div>
                </div>
              </div>

            <div class="card mb-6 shadow" style="width: 300px;">
              <div class="row no-gutters">
                <div class="col-md-4" style="background-color: rgb(51, 51, 51);">
                  <img src="images/categorias.png" class="card-img" style="width: 80%;margin-top:40%;margin-left:10%">
                    </div>
                    <div class="col-md-8">
                    <div class="card-body" style="">
                     <h1 style="font-size: 320%;display: flex;justify-content: center;">{{ $categories }}</h1>
                    <h4 class="card-title" style="font-size:150%;display: flex;justify-content: center;">Categorías</h4>
                    </div>
                  </div>
                </div>
              </div>                  
         </div>   


          </div>
      </div>
  </div>
</div>
</section>


<footer class="clearfix" style="background-image:url({{url('images/footer.jpg')}}); background-position-y: center; background-repeat: no-repeat; background-size: cover; border-top:0px;margin-top:0px;">
  <div class="container pt-5 border-bottom">
    <div class="row">
      <div class="col-md-3 col-sm-12 mb-3 text-center">
        <img src="{{url('images/retina-portfolio.png')}}" style="max-width: 200px; margin-left:-80px;margin-top: 30%;">   
      </div>

      <div class="col-md-9 col-sm-12">

        <div class="col-md-3 col-sm-6 col-6 p-0 float-left mb-3">
        <ul class="list-group" style="color:rgb(245, 206, 13);">
          <li ><a style="color:rgb(245, 206, 13);" href=""><h5 style="color:rgb(245, 206, 13);">INICIO</h5></a></li>
          <li ><a style="color:rgb(245, 206, 13);" href=""><h5 style="color:rgb(245, 206, 13);">NOSOTROS</h5></a></li>
        <li class="list-group-item bg-transparent border-0 p-0 mb-2"><a style="color:rgb(249,249,249);" href="">portafolio</a></li>
          <li ><a style="color:rgb(245, 206, 13);" href=""><h5 style="color:rgb(245, 206, 13);">SERVICIOS</h5></a></li>
          <li ><a style="color:rgb(245, 206, 13);" href=""><h5 style="color:rgb(245, 206, 13);">CONTACTO</h5></a></li>
        </ul>
        </div>

        <div class="col-md-3 col-sm-6 col-6 p-0 mb-3 float-left">
          <h5 class="mb-4 font-weight-bold text-uppercase" style="color:rgb(245, 206, 13);">MONTERREY</h5>
        <ul class="list-group">
        <li class="list-group-item bg-transparent border-0 p-0 mb-2"><img src="{{url('images/phone.png')}}" style="max-width: 20px;"><a style="color:rgb(249,249,249);" href="">  (818)-0-00-72-88</a></li>
          <li class="list-group-item bg-transparent border-0 p-0 mb-2"><img src="{{url('images/ubicacion.png')}}" style="max-width: 20px;"><a style="color:rgb(249,249,249);" href=""> Torre C IOS Campestre <br>Ricardo Margain Zozaya</a></li>
        </ul>
        </div>


        <div class="col-md-3 col-sm-6 col-6 p-0 mb-3 float-left">
          <h5 class="mb-4 font-weight-bold text-uppercase" style="color:rgb(245, 206, 13);">PUEBLA (MATRIZ)</h5>
        <ul class="list-group">
          <li class="list-group-item bg-transparent border-0 p-0 mb-2">
            <img src="{{url('images/world.png')}}" style="max-width: 20px;"><a href="www.animatiomx.com" style="color:rgb(249,249,249);"> www.animatiomx.com</a>
          </li>
          <li class="list-group-item bg-transparent border-0 p-0 mb-2">
            <img src="{{url('images/phone.png')}}" style="max-width: 20px;"><a href=""style="color:rgb(249,249,249);"> (222)-1-78-25-65</a>       
          </li class="list-group-item bg-transparent border-0 p-0 mb-2">
          <li class="list-group-item bg-transparent border-0 p-0 mb-2"> 
            <img src="{{url('images/ubicacion.png')}}" style="max-width: 20px;"><a href="" style="color:rgb(249,249,249);"> 14 Ote. #604 Int.6 <br>
            San Ándres Cholula, Pue.</a>
          </li>
        </ul>
        </div>
      </div>

        <div class="col-md-12">
          <div class="py-4 d-flex" style="display: flex; float: right!important;">
            <a class="mr-4" href="../privacy.html" style="color:rgb(249,249,249);">©2019 Todos los derechos reservados</a>
            <a href="www.animatiomx.com" style="color:rgb(245, 206, 13);">Animatiomx</a>
          </div>
        </div>

    </div>
  </div>
</footer>
        
    </div><!-- #wrapper end -->

    <!-- Go To Top
    ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <!-- External JavaScripts
    ============================================= -->
    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('js/plugins.js')}}"></script>

    <!-- Footer Scripts
    ============================================= -->
    <script src="{{asset('js/functions.js')}}"></script>

</body>
</html>
