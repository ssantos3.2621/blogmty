<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="{{asset('https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i')}}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('css/dark.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('css/font-icons.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('css/animate.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}" type="text/css" />

	<link rel="stylesheet" href="{{asset('css/responsive.css')}}" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<link rel="stylesheet" href="{{asset('css/custom.css')}}" type="text/css" />

	<!-- Document Title
	============================================= -->
	<title>Blog</title>

<style>
.gradient-button {
	background: rgb(131,58,180);
	background: linear-gradient(90deg, rgba(131,58,180,1) 0%, rgba(253,29,29,1) 50%, rgba(252,176,69,1) 100%);
}
</style>

</head>

<body class="stretched" style="border:0px;margin:0px;">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix" >
@include('layouts.header')				
		<section id="page-title" style="background-image:url('images/portada.jpg');max-height:250px;">
			<div class="container clearfix" style="">
				<span style="color: rgb(245, 206, 13)!important;font-size:8rem;margin-left:9%;font-family:'Gasterye'">Blog</span>
			</div>
		</section>

		<section id="content">
			<div class="content-wrap">
				<div class="container clearfix">
		 			<div id="posts" class="post-grid grid-container grid-3 clearfix" data-layout="fitRows">
					@foreach($posts as $post)	
					<div style="overflow: hidden;" class="appear-animation entry clearfix" data-appear-animation="	fadeInUpShorter" data-appear-animation-delay="200" >
				
						<div class="card entry-image shadow" style="background-color:rgb(247, 247, 247);">
						<a href="{{ $post->file}}" data-lightbox="image">
						@if($post->file)
							<img class="card-img-top" src="{{ $post->file}}" alt="Standard Post with Image"style="">
								@endif
						        	<div class="card-body entry-title entry-content"style="">
						            <strong><h2 style="color:rgb(1, 1, 1);font-weight: bold;">{{ $post->name }}</h2></strong>
						            	<ul class="entry-meta clearfix">
										<li><i class="icon-calendar3"></i>{{ $post->created_at }}</li>
										<li><a href="#"><i class="icon-camera-retro"></i></a></li>
										</ul><br>
						            	<span>Por: {{ $post->user->name }}</span><br>
						              	<span>Categoria:  {{ $post->category->name }}</span><br><br>
						            <p>{{str_limit($post->body, 200) }}
									<a href="{{route('post', $post->slug)}}" class="pull-right">Leer más</a></p>
						     		</div>
								</div>
							</div> 
							@endforeach


					</div>

					<div style="display: flex;justify-content: center;">
					{{ $posts->render() }}
					</div>

				</div>
			</div>
		</section>
@include('layouts.footer2')
		
	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script src="{{asset('js/jquery.js')}}"></script>
	<script src="{{asset('js/plugins.js')}}"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="{{asset('js/functions.js')}}"></script>

</body>
</html>