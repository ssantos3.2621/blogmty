@extends('layouts.app')

@section('content')
<div class="container" style="display: flex;justify-content: center;">
<div class="card">
<div class="entry clearfix">

	<!-- Entry Image
	============================================= -->
	<div class="entry-image">
		@if($post->file)
				<img src="{{ $post->file}}" class="img-responsive">
				@endif
	</div><!-- .entry-image end -->


	<!-- Entry Title
	============================================= -->
	<div class="entry-title text-center">
		<h1>{{ $post->name }}</h1>
	</div><!-- .entry-title end -->

	<!-- Entry Meta
	============================================= -->
	<ul class="entry-meta clearfix" style="margin:10px!important;">
		<li><i class="icon-calendar3"></i>{{ $post->created_at }}</li>
		<li><a href="#"><i class="icon-user"></i>Por: {{ $post->user->name }}</a></li>
		<li><i class="icon-folder-open"></i>Categoría
				<a href="{{route('category', $post->category->slug)}}">{{$post->category->name}}</a></li>
		<li><a href="#"><i class="icon-camera-retro"></i></a></li><br><br><br>
	</ul><!-- .entry-meta end -->

	<!-- Entry Content
	============================================= -->
	<div class="entry-content notopmargin" style="margin:10px!important;">

		<p>{!! $post->body !!}	</p>
		<!-- Post Single - Content End -->

		<!-- Tag Cloud
		============================================= -->
		<strong><span>Etiquetas:</span></strong>
		<div class="tagcloud clearfix bottommargin" style="margin:10px!important;">
			@foreach($post->tags as $tag)
				<a href="{{route('tag', $tag->slug)}}">
					{{ $tag->name }}
				</a>
				@endforeach
		</div><!-- .tagcloud end -->

		<div class="clear"></div>

		<!-- Post Single - Share
		============================================= -->
		<div class="si-share noborder clearfix">
			<span>Compartir esta Publicación:</span>
			<div>
				<a href="#" class="social-icon si-borderless si-facebook">
					<i class="icon-facebook"></i>
					<i class="icon-facebook"></i>
				</a>
				<a href="#" class="social-icon si-borderless si-twitter">
					<i class="icon-twitter"></i>
					<i class="icon-twitter"></i>
				</a>
				<a href="#" class="social-icon si-borderless si-pinterest">
					<i class="icon-pinterest"></i>
					<i class="icon-pinterest"></i>
				</a>
				<a href="#" class="social-icon si-borderless si-gplus">
					<i class="icon-gplus"></i>
					<i class="icon-gplus"></i>
				</a>
				<a href="#" class="social-icon si-borderless si-rss">
					<i class="icon-rss"></i>
					<i class="icon-rss"></i>
				</a>
				<a href="#" class="social-icon si-borderless si-email3">
					<i class="icon-email3"></i>
					<i class="icon-email3"></i>
				</a>
				</div>
			</div><!-- Post Single - Share End -->
		</div>
	</div>
	</div>
</div>
@endsection


