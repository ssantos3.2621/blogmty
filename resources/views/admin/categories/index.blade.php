
<!-- <div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">

			<div class="panel panel-default">
				<div class="panel-heading">
					<br>
					<br>
					<br>
					Lista de Categorias		
					<a href="{{route('categories.create')}}" class="btn btn-sm btn-primary pull-right">
						Crear
					</a>		
				</div>
			

				<div class="panel-body">
					<table class="table table-striped table-hover">
					<thead>
						<tr>
						<th width="10px">ID</th>
						<th>NOMBRE</th>
						<th colspan="3">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						@foreach($categories as $category)
						<tr>
							<td>{{$category->id}}</td>
							<td>{{$category->name}}</td>
							<td width="10px">
								<a href="{{route('categories.show', $category->id)}}" class="btn btn-sm btn-default">ver</a>
							</td>
							<td width="10px">
								<a href="{{route('categories.edit', $category->id)}}" class="btn btn-sm btn-default">editar</a>
							</td>
							<td width="10px">
								{!! Form::open(['route' => ['categories.destroy',$category->id], 'method'=>'DELETE'])!!}
									<button class="btn btn-sm btn-danger">
										Eliminar
									</button>
								{!! Form::close()!!}
							</td>
						</tr>
						@endforeach
					</tbody>
					</table>
					{{$categories->render()}}
				</div>
			</div>


		</div>
	</div>
</div> -->


<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets
    ============================================= -->
    <link href="{{asset('https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/dark.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/font-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/animate.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}" type="text/css" />

    <link rel="stylesheet" href="{{asset('css/responsive.css')}}" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="stylesheet" href="{{asset('css/custom.css')}}" type="text/css" />

    <!-- Document Title
    ============================================= -->
    <title>Blog</title>

<style>
.gradient-button {
    background: rgb(131,58,180);
    background: linear-gradient(90deg, rgba(131,58,180,1) 0%, rgba(253,29,29,1) 50%, rgba(252,176,69,1) 100%);
}
</style>

</head>

<body class="stretched" style="border:0px;margin:0px;">

    <!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix" >

 @include('layouts.header')
    
<section id="content">

<div class="content-wrap nopadding">

 <div class="section full-screen nopadding nomargin" style="background: url('images/ondas1.png')
    center center no-repeat;background-size: cover;">
      <div class="container-fluid vertical-middle divcenter clearfix">
          <div class="row d-flex justify-content-center" style="font-size: 100%;">

          	<!--contenido--->

			<div class="container text-center"  style="margin-top:2%;margin-bottom: -4%;">
				<form action="{{route('eliminar1')}}" method="post">
    {{ csrf_field() }}
     <input type="hidden" name="_method" value="delete">
			<span style="font-family:'Gasterye';font-size:6rem;" class="card-title">Categorias&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
			  <a href="{{route('categories.create')}}" class=""><img src="{{url('images/agregar.png')}}" style="max-width:30px;"></a>
			  <a href="" class=""><img src="{{url('images/editar.png')}}" style="max-width:30px;"></a>
			<!--   <a href="" class=""><img src="{{url('images/eliminar.png')}}" style="max-width:30px;"></a> -->
			  <button type="submit" name="borrar" style="border:none;background-color:transparent;"><img src="{{url('images/eliminar.png')}}" style="max-width:30px;max-height:30px"></button>
			
			</div>
      		<div class="card col-md-8 col-sm-11 col-lg-6" style="border-radius:20px;">
				<div class="card-body">
					<table class="table">
					<table class="table">
					<thead>
						<tr>
						<th style="border:none;">Nombre de la categoria</th>
						<th style="border:none;">No. entradas</th>
						
						<!-- <th colspan="3">&nbsp;</th> -->
						</tr>
					</thead>
					<tbody>
						@foreach($categories as $category)
						<tr>
							<td>{{$category->name}}</td>
							<td class="text-center">{{$category->id}}</td>
							<td> <input type="checkbox" id="action" name="eliminar[]" value="{{$category->id}}"></td>
							
						</tr>
						@endforeach
					</tbody>
					</table>
					<div style="display: flex;justify-content: center;">
					{{$categories->render()}}
				</div>
				</div>
			</div>
			</form>

		<!-- fin del contenido--->

          </div>
      </div>
  </div>
</div>
</section>

@include('layouts.footer2')    

    </div><!-- #wrapper end -->

    <!-- Go To Top
    ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <!-- External JavaScripts
    ============================================= -->
    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('js/plugins.js')}}"></script>

    <!-- Footer Scripts
    ============================================= -->
    <script src="{{asset('js/functions.js')}}"></script>

</body>
</html>
