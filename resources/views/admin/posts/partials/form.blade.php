{{ Form::hidden('user_id', auth()->user()->id) }}

<div class="form-group" style="display: flex;margin:0;">
	{{ Form::label('name', 'Titulo:&nbsp;')}}
	{{ Form::text('name', null, ['class'=>'form-control', 'id'=>'name'])}}
</div>
<br>
<div class="form-group" style="display: flex;margin:0;">
	{{ Form::label('slug', 'Slug:&nbsp;&nbsp;&nbsp;&nbsp;')}}
	{{ Form::text('slug', null, ['class'=>'form-control', 'id'=>'slug'])}}
	
</div>
<br>
<div class="form-group" style="display: flex;margin:0;">
	{{ Form::label('body', 'Texto:&nbsp;&nbsp;')}}
	{{ Form::textarea('body', null, ['class'=>'form-control','rows'=>'2'])}}
	
</div>
	<br>
<div class="form-group" style="display: flex;margin:0;">

	{{ Form::label('category_id', 'Categoria:&nbsp;')}}
	{{ Form::select('category_id', $categories, null, ['class' => 'form-control'])}}
	
</div>
<br>
<div class="form-group" style="display: flex;margin:0;">
@php
    if(isset($post)) {
        $tag = $post->tags->pluck('name')->all();
    } else {
        $tag = null;
    }
@endphp
    {!! Form::label('tags', 'Etiqueta:&nbsp;&nbsp;&nbsp;', ['class' => 'control-label']) !!}
    {!! Form::select('tags[]', $tags, $tag, ['class' => 'form-control', 'required', 'multiple','size'=>'3']) !!}
</div>
<br>
<div class="form-group" style="display: flex;margin:0;">
	{{ Form::label('file', 'Imagen:&nbsp;&nbsp;')}}
	{!! Form::file('file') !!}
</div>
<br>
<div class="form-group" style="display: flex;margin:0;justify-content:center;">
	<!-- {{ Form::submit('Crear', ['class'=>'btn btn-sm btn-dark'])}} -->
	<button type="submit" class="btn btn-sm btn-dark"><img src="{{url('images/enviar.png')}}" style="width:15px;">&nbsp;&nbsp;&nbsp;Crear</button>
</div>

@section('scripts')
<script src="{{asset('vendor/stringtoslug/jquery.stringToSlug.min.js')}}"></script>
<script>
	$(document).ready(function(){
		$("#name, #slug").stringToSlug({
			callback:function(text){
				$("#slug").val(text);
			}
		});
	});
</script>
@endsection