<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets
    ============================================= -->
    <link href="{{asset('https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/dark.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/font-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/animate.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}" type="text/css" />

    <link rel="stylesheet" href="{{asset('css/responsive.css')}}" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="stylesheet" href="{{asset('css/custom.css')}}" type="text/css" />

    <!-- Document Title
    ============================================= -->
    <title>Blog</title>

<style>
.gradient-button {
    background: rgb(131,58,180);
    background: linear-gradient(90deg, rgba(131,58,180,1) 0%, rgba(253,29,29,1) 50%, rgba(252,176,69,1) 100%);
}
</style>

</head>

<body class="stretched" style="border:0px;margin:0px;">

    <!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix">
 @include('layouts.header')
<section id="content" >

<div class="content-wrap nopadding" >

 <div class="section full-screen nopadding nomargin" style="background:url({{url('images/ondas1.png')}})
    center center no-repeat;background-size: cover;" >
      <div class="container-fluid vertical-middle divcenter clearfix">
          <div class="row justify-content-center" style="font-size: 100%;">

          	<!--contenido--->
          	
			<div class="container text-center" style="margin-top:2%;margin-bottom:-2%">
			<span style="font-family:'Gasterye';font-size:6rem;" class="card-title">Crear Publicación</span>
			</div>
      		<div class="card col-md-8 col-sm-11 col-lg-6" style="border-radius:20px;">
				<div class="card-body">

					{!!Form::open(['route' =>'posts.store', 'files' => true])!!}

						@include('admin.posts.partials.form')
						
					{!!Form::close()!!}


				</div>

			</div>

		<!-- fin del contenido--->

          </div>
      </div>
  </div>
</div>
</section>

@include('layouts.footer2')    

    </div><!-- #wrapper end -->

    <!-- Go To Top
    ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <!-- External JavaScripts
    ============================================= -->
    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('js/plugins.js')}}"></script>

    <!-- Footer Scripts
    ============================================= -->
    <script src="{{asset('js/functions.js')}}"></script>

</body>
</html>
