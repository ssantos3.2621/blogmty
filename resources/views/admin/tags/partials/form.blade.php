<div class="container">


<div class="form-group">
	{{ Form::label('name', 'Nombre de la etiqueta')}}
	{{ Form::text('name', null, ['class'=>'form-control', 'id'=>'name'])}}
</div>
<div class="form-group">
	{{ Form::label('slug', 'URL Amigable')}}
	{{ Form::text('slug', null, ['class'=>'form-control', 'id'=>'slug'])}}
</div>
<div class="form-group d-flex justify-content-center">
	<!-- {{ Form::submit('Guardar', ['class'=>'btn btn-sm btn-dark'])}} -->
	<button type="submit" class="btn btn-sm btn-dark"><img src="{{url('images/guardar.png')}}" style="width:15px;">&nbsp;&nbsp;&nbsp;Guardar</button>
</div>
</div>
@section('scripts')
<script src="{{asset('vendor/stringtoslug/jquery.stringToSlug.min.js')}}"></script>
<script>
	$(document).ready(function(){
		$("#name, #slug").stringToSlug({
			callback:function(text){
				$("#slug").val(text);
			}
		});
	});
</script>
@endsection