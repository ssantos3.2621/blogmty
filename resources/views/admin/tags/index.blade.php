<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets
    ============================================= -->
    <link href="{{asset('https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/dark.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/font-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/animate.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}" type="text/css" />

    <link rel="stylesheet" href="{{asset('css/responsive.css')}}" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="stylesheet" href="{{asset('css/custom.css')}}" type="text/css" />

    <!-- Document Title
    ============================================= -->
    <title>Blog</title>

<style>
.gradient-button {
    background: rgb(131,58,180);
    background: linear-gradient(90deg, rgba(131,58,180,1) 0%, rgba(253,29,29,1) 50%, rgba(252,176,69,1) 100%);
}
</style>

</head>

<body class="stretched" style="border:0px;margin:0px;">

    <!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix" >

 @include('layouts.header')
    
<section id="content">

<div class="content-wrap nopadding">

 <div class="section full-screen nopadding nomargin" style="background: url('images/ondas1.png')
    center center no-repeat;background-size: cover;">
      <div class="container-fluid vertical-middle divcenter clearfix">
          <div class="row d-flex justify-content-center" style="font-size: 100%;">

          	<!--contenido--->

			<div class="container text-center" style="margin-top:2%;margin-bottom: -4%;">
				<form action="{{route('eliminar2')}}" method="post">
    {{ csrf_field() }}
     <input type="hidden" name="_method" value="delete">
			<span style="font-family:'Gasterye';font-size:6rem;" class="card-title">Etiquetas&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> 
			  <a href="{{route('tags.create')}}" class=""><img src="{{url('images/agregar.png')}}" style="max-width:30px;"></a>
			  <a href="" class=""><img src="{{url('images/editar.png')}}" style="max-width:30px;"></a>
			  <!-- <a href="" class=""><img src="{{url('images/eliminar.png')}}" style="max-width:30px;"></a> -->
			  <button type="submit" name="borrar" style="border:none;background-color:transparent;"><img src="{{url('images/eliminar.png')}}" style="max-width:30px;max-height:30px"></button>
			
			</div>
      		<div class="card col-md-8 col-sm-11 col-lg-6" style="border-radius:20px;">
				<div class="card-body">
					<table class="table">
					<thead>
						<tr>
						<h5>Nombre de la Etiqueta</h5>
						
						<!-- <th colspan="3">&nbsp;</th> -->
						</tr>
					</thead>
					<tbody>
						@foreach($tags as $tag)
						<tr>
							<td>{{$tag->name}}</td>
							<td> <input type="checkbox" id="action" name="eliminar[]" value="{{$tag->id}}"></td>
							<!-- <td width="">
								<a href="{{route('tags.show', $tag->id)}}" class="btn btn-sm btn-default">ver</a>
							</td>
							<td width="">
								<a href="{{route('tags.edit', $tag->id)}}" class="btn btn-sm btn-default">editar</a>
							</td>
							<td width="">
								{!! Form::open(['route' => ['tags.destroy',$tag->id], 'method'=>'DELETE'])!!}
									<button class="btn btn-sm btn-danger">
										Eliminar
									</button>
								{!! Form::close()!!}
							</td> -->
						</tr>
						@endforeach
					</tbody>
					</table>
					<div style="display: flex;justify-content: center;">
					{{$tags->render()}}
				</div>
				</div>
			</div>
</form>

		<!-- fin del contenido--->

          </div>
      </div>
  </div>
</div>
</section>

@include('layouts.footer2')    

    </div><!-- #wrapper end -->

    <!-- Go To Top
    ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <!-- External JavaScripts
    ============================================= -->
    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('js/plugins.js')}}"></script>

    <!-- Footer Scripts
    ============================================= -->
    <script src="{{asset('js/functions.js')}}"></script>

</body>
</html>
