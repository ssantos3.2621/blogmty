<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\post;
use App\category;

class PageController extends Controller
{
    public function blog(){

    	$posts = post::orderBy('id', 'DESC')->paginate(6);
    	return view('web.posts', compact('posts'));	
    }

    public function category($slug)
    {
        $category = category::where('slug', $slug)->pluck('id')->first();
        $posts = post::where('category_id', $category)->orderBy('id', 'DESC')->paginate(3);
        return view('web.posts', compact('posts'));
    }

    public function tag($slug){
        $posts = post::whereHas('tags', function($query) use($slug){
            $query->where('slug', $slug);
        })

        ->orderBy('id', 'DESC')->paginate(3);
        return view('web.posts', compact('posts'));
    }

    public function post($slug)
    {
    	$post = post::where('slug', $slug)->first();
    	
    	return view('web.post', compact('post'));
    }
}
