<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tag;
use App\post;
use App\category;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts      = post::count();
        $tags       = tag::count();
        $categories = category::count();

        return view('home', get_defined_vars());
    }
}
