<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\PostStoreRequest;
use App\Http\Requests\PostUpdateRequest;

use Illuminate\Support\Facades\Storage;


use App\post;
use App\pategory;
use App\tag;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = post::orderBy('id', 'DESC')->paginate(5);

        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $categories = category::orderBy('name', 'ASC')->pluck('name', 'id');
        // $tags = Tag::orderBy('name','ASC')->get();
        $tags = tag::pluck('name', 'name')->all();

        return view('admin.posts.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostStoreRequest $request)
    {
        //validar
        $post = post::create($request->all());

        //image
        // if($request->file('file')){
        //     $path = Storage::disk('public')->put('image',  $request->file('file'));
        //     $post->fill(['file' => asset($path)])->save();
        // }

        if($request->file('file')){
            $path = Storage::disk('public')->put('image',  $request->file('file'));
            $post->fill(['file' => asset($path)])->save();
        }

        $post->tags()->attach($request->get('tags'));

        return redirect()->route('posts.edit', $post->id)->with('info','Publicación creada con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = post::find($id);

        return view('admin.posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = category::orderBy('name', 'ASC')->pluck('name', 'id');
        $tags = tag::orderBy('name','ASC')->get();

        $post = post::find($id);

        return view('admin.posts.edit', compact('post', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostUpdateRequest $request, $id)
    {
        $post = post::find($id);

        $post->fill($request->all())->save();

        //image
        if($request->file('file')){
            $path = Storage::disk('public')->put('image', $request->file('file'));
            $post->fill(['file' => asset($path)])->save();
        }

        $post->tags()->sync($request->get('tags'));

        return redirect()->route('posts.edit', $post->id)->with('info','Publicación actualizada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $post = post::find($id)->delete();
   
        return back()->with('info', 'Eliminado correctamente');
    }

public function eliminar(Request $request)
    {
       // $post = Post::find($id)->delete();
       $post = $request->input('eliminar',[]);
      Post::whereIn("id",$post)->delete(); 
            // $request->all();

        // Post::whereIn('id', $request->input('eliminar'))->delete();


        return back()->with('info', 'Eliminado correctamente');
    }


}
