<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', 'blog');

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::get('blog', 'Web\PageController@blog')->name('blog');
Route::get('blog/{slug}', 	'Web\PageController@post')->name('post');
Route::get('category/{slug}', 'Web\PageController@category')->name('category');
Route::get('tag/{slug}', 'Web\PageController@tag')->name('tag');

//admin
Route::resource('tags','Admin\TagController');
Route::resource('categories','Admin\CategoryController');
Route::resource('posts','Admin\PostController');

Route::delete('/eliminar', 'Admin\PostController@eliminar')->name('eliminar');
Route::delete('/eliminar1', 'Admin\CategoryController@eliminar')->name('eliminar1');
Route::delete('/eliminar2', 'Admin\TagController@eliminar')->name('eliminar2');
// Route::delete('/deleteall','Purchase_detailsController@deleteall')->name('deleteall');

Route::get('/clear', function() {
    
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('view:clear');
return "Cleared!";

});
